Summary
-------
This module provides functionality to intercept any outgoing email of a website.
It allows for all outgoing mails to be redirected to a single email address.
Every TO, CC or BCC email address will be ignored if the module is enabled.
You can set the redirection email address setting in your settings.php or in the
administration interface. Original email address information can be included at
the bottom of the mail if the option is enabled in the administration interface.

Although this module has been made for development purposes, this module
is safe to use on a production site.

Configuration
-------------
To enable and configure this module do the following:

1. Go to Admin -> Modules, and enable Devel Mail.

2. Go to Admin -> Configuration -> Development -> Devel Mail Settings, and make
   any necessary configuration changes.

Settings.php
------------
You can use the variable $conf['develmail_email_address'] in the settings.php
to set the forwarding email address as well. Be aware that this settings will be
overwritten if an email address is set in the administrative interface of the
module!
