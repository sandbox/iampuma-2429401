<?php
/**
 * @file
 * Contains all admin forms and pages for the environment module.
 */

/**
 * Environment module configuration callback.
 */
function develmail_settings() {
  // Provide possibility to override the settings.php configuration setting.
  $environment_variable = variable_get('develmail_email_address');
  $environment_variable_override = variable_get('develmail_email_adress_override');
  $detection = !empty($environment_variable) ? t('Detected:') . ' ' . variable_get('develmail_email_address') : t('No environment mail setting detected!');
  $overriden = !empty($environment_variable_override) ? ' ' . t('(overriden)') : '';
  $form['develmail_email_adress_override'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#description' => t('By default the mail used is the $conf[\'develmail_email_address\'] defined in your settings.php. Override if needed.') . '<br />' .  $detection . $overriden,
    '#default_value' => !empty($environment_variable_override) ? variable_get('develmail_email_adress_override') : '',
  );

  // Provide possibility setting to attach detailed overview of overwritten mail settings.
  $form['develmail_email_details'] = array(
    '#type' => 'select',
    '#title' => t('Override details'),
    '#description' => t('Provides additional information of overrides in the footer of every mail.'),
    '#options' => array(
      0 => t('disabled'),
      1 => t('enabled'),
    ),
    '#default_value' => variable_get('develmail_email_details', 1),
  );

  return system_settings_form($form);
}
